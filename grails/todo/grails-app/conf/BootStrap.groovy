import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        try {
            def roleAdmin = new Role(authority: "ROLE_ADMIN").save()
            def roleBasic = new Role(authority: "ROLE_BASIC").save()

            def admin = new User(name: "Pepe", surnames: "Pepon", confirmPassword: "admin", email:"pepon@pepe.com", username: "admin", password: "admin").save()
            def user1 = new User(name: "Nacho", surnames: "Reando", confirmPassword: "usuario1", email:"nacho@mail.com", username: "usuario1", password: "usuario1").save()
            def user2 = new User(name: "Normi", surnames: "Figura", confirmPassword: "usuario2", email:"normi@mail.com", username: "usuario2", password: "usuario2").save()

            PersonRole.create admin, roleAdmin
            PersonRole.create user1, roleBasic
            PersonRole.create user2, roleBasic

            def categoryHome = new Category(name: "Hogar").save()
            def categoryJob = new Category(name: "Trabajo").save()

            def tagEasy = new Tag(name: "Fácil").save()
            def tagDifficult = new Tag(name: "Difícil").save()
            def tagArt = new Tag(name: "Arte").save()
            def tagRoutine = new Tag(name: "Rutina").save()
            def tagKitchen = new Tag(name: "Cocina").save()

            def todoPaintKitchen = new Todo(title: "Pintar cocina", date: new Date() + 1, completada: true)
            def todoCollectPost = new Todo(title: "Recoger correo postal", date: new Date() + 2, completada: false)
            def todoBakeCake = new Todo(title: "Cocinar pastel", date: new Date() + 4, completada: true)
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", date: new Date(), completada: false)

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.user = user1
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.user = user1
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.user = user2
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.user = user2
            todoWriteUnitTests.save()


        }catch(Exception ex){

        }
    }

    def destroy = {}

}

