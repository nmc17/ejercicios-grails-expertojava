class UrlMappings {

	static mappings = {

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/todos/$username"(controller:"todo",action:"showtodosbyuser")

        "/"(view:"/index")
        "500"(view:'/error')


    }
}
