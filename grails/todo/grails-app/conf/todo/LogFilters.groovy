package todo

class LogFilters {

    def springSecurityService

    def filters = {
        all(controller:'todo|category|tag|user', action:'*') {
            before = {

            }
            after = { Map model ->
                def user = springSecurityService.currentUser
                log.trace("User " + user.username + " - Controlador ${controllerName} - Accion ${actionName} - Modelo ${model}")
            }
            afterView = { Exception e ->

            }
        }
    }
}


