package es.ua.expertojava.todo

class Tag {

    String color = "#FFFFFF"
    String name

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false, nullable:false, unique:true)
        color (nullable: true, shared: "rgbcolor")
    }

    String toString(){
        name
    }
}

