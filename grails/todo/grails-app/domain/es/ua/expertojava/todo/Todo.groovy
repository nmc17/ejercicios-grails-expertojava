package es.ua.expertojava.todo

class Todo {
    String title
    String description
    Date date
    Date reminderDate
    Boolean completada = false
    String url
    Category category
    User user
    Date dateDone
    Date dateCreated
    Date lastUpdated

    static searchable = [only: ['title']]

    static hasMany = [tags:Tag]
    static belongsTo = [Tag]

    static constraints = {
        title(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false)
        reminderDate(nullable:true,
                validator: { val, obj ->
                    if (val && obj.date) {
                        return val.before(obj?.date)
                    }
                    return true
                }
        )

        url(nullable:true, url:true)
        category(nullable:true)
        dateDone(nullable: true)
    }

    String toString(){
        title
    }

}

