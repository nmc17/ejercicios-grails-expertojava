package es.ua.expertojava.todo

import grails.transaction.Transactional


class CategoryService {

        def delete(Category categoryInstance) {


            def categoryes = categoryInstance.todos.findAll()

            categoryes.each {todo ->
                categoryInstance.removeFromTodos(todo)
            }

            categoryInstance.delete flush:true

        }
}
