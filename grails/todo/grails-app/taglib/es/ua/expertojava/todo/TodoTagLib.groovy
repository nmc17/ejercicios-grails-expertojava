package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = 'todo'

    def printIconFromBoolean  = {attrs ->

        if(attrs["value"]) {
            out << asset.image(src:"completed.png")
        }
        else{
            out << asset.image(src:"uncompleted.png")
        }
    }

    def includeJs = {attrs ->
        out << "<script src='scripts/${attrs['script']}.js'></script>"
    }


}
