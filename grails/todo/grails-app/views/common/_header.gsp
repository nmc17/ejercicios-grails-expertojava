<div id="header">
    <div id="menu">
        <sec:ifNotLoggedIn>
            <g:link controller="login" action="auth">Login</g:link>
        </sec:ifNotLoggedIn>
        <sec:ifLoggedIn>
            Welcome <sec:username/>
            <g:form controller="logout">
                <g:submitButton name="logout" action="index" method="post">Logout</g:submitButton>
            </g:form>
        </sec:ifLoggedIn>
    </nobr>
    </div>
</div>


