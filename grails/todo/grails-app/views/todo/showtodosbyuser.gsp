<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    </ul>
</div>
<div id="list-todo" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'Title')}" />

            <th><g:message code="todo.category.label" default="Category" /></th>

            <g:sortableColumn property="done" title="${message(code: 'todo.done.label', default: 'Done')}" />

        </tr>
        </thead>
        <tbody>
        <g:each in="${todoInstanceList}" status="i" var="todoInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td>${fieldValue(bean: todoInstance, field: "title")}</td>

                <td>${fieldValue(bean: todoInstance, field: "category")}</td>

                <td><todo:printIconFromBoolean value="${todoInstance.completada}"/></td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${todoInstanceCount ?: 0}" />
    </div>
</div>
</body>
</html>