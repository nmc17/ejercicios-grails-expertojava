package es.ua.expertojava.todo

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
@Mock(Todo)
class TodoServiceSpec extends Specification {

    def service = new TodoService()

    def setup() {
    }

    def cleanup() {
    }

    void "El método getNextTodos devuelve los siguientes todos de los días pasado por parámetro"() {
        given:
        def usuario = new User(username: "usuario1", password: "usuario1", name: "usuario1", surnames: "usuario1", email: "usuario1@gmail.com", confirmPassword: "usuario1")

        def todoDayBeforeYesterday = new Todo(title:"Todo day before yesterday", date: new Date() - 2, user: usuario)
        def todoYesterday = new Todo(title:"Todo yesterday", date: new Date() - 1, user: usuario )
        def todoToday = new Todo(title:"Todo today", date: new Date(), user: usuario)
        def todoTomorrow = new Todo(title:"Todo tomorrow", date: new Date() + 1, user: usuario )
        def todoDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 2, user: usuario )
        def todoDayAfterDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 3, user: usuario )

        and:
        mockDomain(Todo,[todoDayBeforeYesterday, todoYesterday, todoToday, todoTomorrow, todoDayAfterTomorrow, todoDayAfterDayAfterTomorrow])
        and:
        def nextTodos = service.getNextTodos(2,[:])
        expect:
        Todo.count == 6
        and:
        nextTodos.containsAll([todoTomorrow, todoDayAfterTomorrow])
        nextTodos.size() == 2
        and:
        !nextTodos.contains(todoDayBeforeYesterday)
        !nextTodos.contains(todoToday)
        !nextTodos.contains(todoYesterday)
        !nextTodos.contains(todoDayAfterDayAfterTomorrow)
    }

    void "El método countNextTodos devuelve el número de todos de los siguientes días pasado por parámetro"() {
        given:
        def usuario = new User(username: "usuario1", password: "usuario1", name: "usuario1", surnames: "usuario1", email: "usuario1@gmail.com", confirmPassword: "usuario1")

        def todoYesterday = new Todo(title:"Todo yesterday", date: new Date() - 1, user: usuario )
        def todoToday = new Todo(title:"Todo today", date: new Date(), user: usuario)
        def todoTomorrow = new Todo(title:"Todo tomorrow 1.0", date: new Date() + 1, user: usuario )
        def todoTomorrow2 = new Todo(title:"Todo tomorrow 2.0", date: new Date() + 1, user: usuario )

        and:
        mockDomain(Todo,[todoYesterday, todoToday, todoTomorrow, todoTomorrow2])
        when:
        def countNextTodos = service.countNextTodos(1)
        then:
        countNextTodos == 2
    }

    void "El método saveTodo almacena la nueva instancia y da valor a la fecha de realización cuando la tarea ha sido realizada"() {
        given:
        def usuario = new User(username: "usuario1", password: "usuario1", name: "usuario1", surnames: "usuario1", email: "usuario1@gmail.com", confirmPassword: "usuario1")

        def todo = new Todo(title:"Todo", date: new Date(), user: usuario )

        and:
        mockDomain(Todo,[todo])
        when:"Cuando la tarea no ha sido realizada"
        service.saveTodo(todo, usuario)
        then:
        todo.dateDone == null
        when:"cuando la tarea ha sido realizada"
        todo.setCompletada(true)
        service.saveTodo(todo, usuario)
        then:
        todo.dateDone != null
    }

    void "El método lastTodosDone devuelve las últimas tareas realizadas en el intervalo de horas indicado"() {
        given:
        def usuario = new User(username: "usuario1", password: "usuario1", name: "usuario1", surnames: "usuario1", email: "usuario1@gmail.com", confirmPassword: "usuario1")

        def todoYesterday = new Todo(title:"Todo yesterday", date: new Date() - 1, user: usuario )
        def todoNowDone = new Todo(title:"Todo today", date: new Date(), user: usuario, done: true, dateDone: new Date())
        def todoNowNotDone = new Todo(title:"Todo tomorrow", date: new Date(), user: usuario )
        def todoNowDone2 = new Todo(title:"Todo tomorrow 2", date: new Date(), user: usuario, done: true, dateDone: new Date())
        def todoNowNotDone2 = new Todo(title:"Todo tomorrow 3", date: new Date(), user: usuario )
        def todoTomorrow = new Todo(title:"Todo tomorrow", date: new Date() + 1, user: usuario )

        and:
        mockDomain(Todo,[todoYesterday, todoNowDone, todoNowDone2, todoNowNotDone, todoNowNotDone2, todoTomorrow])
        when:"Si la tarea no ha sido realizada"
        def lastTodos = service.lastTodosDone(2)
        then:
        lastTodos.containsAll([todoNowDone, todoNowDone2])
        lastTodos.size() == 2
        and:
        !lastTodos.contains(todoYesterday)
        !lastTodos.contains(todoNowNotDone)
        !lastTodos.contains(todoNowNotDone2)
        !lastTodos.contains(todoTomorrow)
    }


}