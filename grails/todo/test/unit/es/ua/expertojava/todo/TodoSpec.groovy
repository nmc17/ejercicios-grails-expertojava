package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    def "Si la fecha de recordatorio nunca puede ser posterior a la fecha de la propia tarea"() {
        given:
        def t1 = new Todo(title: "Prueba test", date: new Date(), reminderDate:new Date() +1,  completada: true)
        when:
        t1.validate()
        then:
        t1?.errors['reminderDate']
    }
}
