class Todo {
     String titulo
     String descripcion

}

def todos = [new Todo(titulo:"Lavadora",descripcion:"Poner lavadora"), new Todo(titulo:"Impresora",descripcion:"Comprar cartuchos impresora"), new Todo(titulo:"Películas",descripcion:"Devolver películas videoclub")]

for (Todo in todos) {    
      println("${Todo.getTitulo()}" + " ${Todo.getDescripcion()}")
}

