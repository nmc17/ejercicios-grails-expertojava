/* Añade aquí la implementación del factorial en un closure */
def factorial

factorial = { n ->
    return (n==0) ? 1 : n * factorial(n-1)
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

def lista = [1,2,3,4,5,6,,7,8,9,10]

for (elem in lista){
    println "Factorial de " + elem + ": " + factorial(elem)
}


def ayer

ayer = { fecha ->
    return fecha - 1
}

def manyana

manyana = { fecha ->
    return fecha + 1
}

def fechaPruebaHoy = new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20")
def fechaPruebaAyer = new Date().parse("d/M/yyyy H:m:s","27/6/2008 00:30:20")
def fechaPruebaManyana = new Date().parse("d/M/yyyy H:m:s","29/6/2008 00:30:20")

assert ayer(fechaPruebaHoy) == fechaPruebaAyer
assert manyana(fechaPruebaHoy) == fechaPruebaManyana


def listaFechas = [new Date().parse("d/M/yyyy H:m:s","27/6/2008 00:30:20"),
                 new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20"),
                 new Date().parse("d/M/yyyy H:m:s","29/6/2008 00:30:20"),
                 new Date().parse("d/M/yyyy H:m:s","30/6/2008 00:30:20")]
                 
for (fecha in listaFechas){
    println "La fecha de ayer de " + fecha + ": " + ayer(fecha) 
    println "La fecha de mañana de " + fecha + ": " + manyana(fecha) + "\n"
}
    