/*CALCULADORA*/

class Calculadora{
    
    def suma(a,b){
        return a+b
    }
    
    def resta(a,b){
        return a-b
    }
    
    def multiplicacion(a,b){
        return a*b
    }
    
    def division(a,b){
        return a / b
    }
}

System.in.withReader {
  print 'Introduzca el primer parametro: '
  a = it.readLine()
  println(a)
  
  print 'Introduzca el segundo parametro: '
  b = it.readLine()
  println(b)
  
  print 'Introduzca operador: '
  op1 = it.readLine()
  println(op1)
}

def Calculadora = new Calculadora()
def resultado
def aInt
def bInt

aInt = a.toInteger()
bInt = b.toInteger()

if(op1 == "+"){
    resultado = Calculadora.suma(aInt,bInt)
    println "El resultado de la suma es " + (resultado)
    
}
else if(op1 == "-"){
    resultado = Calculadora.resta(aInt,bInt)
    println "El resultado de la resta es " + (resultado)
    
}
else if(op1 == "*"){
    resultado = Calculadora.multiplicacion(aInt,bInt)
    println "El resultado de la multiplicacion es " + (resultado)
    
}
else if(op1 == "/"){
    resultado = Calculadora.division(aInt,bInt)
    println "El resultado de la division es " + (resultado)
    
}
else{
    println "Operando no existente"
}